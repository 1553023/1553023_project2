1553023
Trần Gia Trọng Nhân
15CLC
email: 1553023@student.hcmus.edu.vn

Visual 2017 chạy chương trình ở  x86

Đã thực hiện:

a. Khi user chọn các menu hay toolbar button thực hiện được các yêu cầu tương ứng. 
b. Color và Font (1đ): 
	 Color: mở hộp thoại Color cho phép user chọn màu vẽ.
	 Font: mở hộp thoại Font, cho phép user chọn font, size, style (bold, italic, underline). 
	 Color và Font được lưu trữ riêng cho mỗi child window. 

c. Draw (2đ): 
	Line: cho phép user dùng mouse vẽ đoạn thẳng trên child window. Màu vẽ là màu đã chọn ở chức năng Color.
	Rectangle, Ellipse: tương tự như Line, nhưng vẽ hình chữ nhật hay ellipse. o 
	Text: cho phép user tạo 1 chuỗi text lên child window. 
	Màu và font của text là màu và font đã chọn ở chức năng Color, Font. 
	Text được hiển thị tại vị trí click mouse.
	Các object đã vẽ (line, rectangle, ellipse, text) được lưu trữ riêng cho mỗi child window. 
d. Paint lại child window (1đ): vẽ lại các object cho child window khi nhận được message WM_PAINT.
e. Edit các object (2đ): 
	User có thể chỉnh sửa các object bằng cách click mouse để chọn object và kéo/thả để chỉnh sửa kích thước hay di chuyển vị trí của object.  chưa edit được text.
f. Save (1đ): 
	 Nếu child window chưa save (tên file mặc định=”Noname-*.drw”) thì mở dialog Save as để user nhập tên file và lưu data của child window vào file.
	 Nếu child window đã save rồi thì lưu data của child window vào file. 
g. Open (1đ): Mở dialog Open để user chọn file *.drw. o Đọc file và hiển thị các data lên window. 

Hướng dẫn cách edit:

khi muốn edit muốn phóng to hay thu nhỏ object thì ta nhấn giữ chuột trái vào 1 góc của hình và di chuyển chuột để edit. 

muốn di chuyển hình thì ta nhấn giữ chuột phải vào hình và di chuyển chuột

khi tắt 1 cửa sổ con, sẽ có hộp thoại hỏi save, nếu chọn ok và chưa save lần nào thì mở save dialog box, nếu đã save thì save vào file đã save.