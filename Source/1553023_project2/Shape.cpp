#include "stdafx.h"
#include "Shape.h"


Point newPoint(int x, int y)
{
	Point p;
	p.x = x;
	p.y = y;

	return p;
}

COLORREF CShape::getColor()
{
	return this->color;
}

void CShape::setType(ShapeMode type)
{
	this->type = type;
}

RECT* CShape::getDimens()
{
	RECT* rect = new RECT();
	rect->left = x1;
	rect->top = y1;
	rect->right = x2;
	rect->bottom = y2;

	return rect;
}

void CShape::setDimens(RECT* rect)
{
	this->x1 = rect->left;
	this->x2 = rect->right;
	this->y1 = rect->top;
	this->y2 = rect->bottom;
}

void CShape::setColor(COLORREF color)
{
	this->color = color;
}

int CShape::getPenStyle()
{
	return penStyle;
}




CShape* CEllipse::Create()
{
	return new CEllipse;
}

void CEllipse::SetData(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, COLORREF color, int penStyle)
{
	x1 = nLeftRect;
	y1 = nTopRect;
	x2 = nRightRect;
	y2 = nBottomRect;

	this->type = DRAWELLIPSE;
	this->color = color;
	this->penStyle = penStyle;
	

}

void CEllipse::Draw(HDC hdc)
{
	if (x1 < x2)
	{
		int tmp = x1;
		x1 = x2;
		x2 = tmp;
	}
	if (y1 < y2)
	{
		int tmp = y1;
		y1 = y2;
		y2 = tmp;
	}

	Ellipse(hdc, x1, y1, x2, y2);
}

ShapeMode CEllipse::getType()
{
	return this->type;
}


void CLine::SetData(int a, int b, int c, int d, COLORREF color, int penStyle)
{
	x1 = a;
	y1 = b;
	x2 = c;
	y2 = d;

	this->type = LINE;
	this->color = color;
	this->penStyle = penStyle;
	
}

void CLine::Draw(HDC hdc)
{
	MoveToEx(hdc, x1, y1, NULL);
	LineTo(hdc, x2, y2);
}

CShape* CLine::Create()
{
	return new CLine;
}

ShapeMode CLine::getType()
{
	return this->type;
}


CShape* CRectangle::Create()
{
	return new CRectangle;
}


void CRectangle::SetData(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, COLORREF color, int penStyle)
{
	x1 = nLeftRect;
	y1 = nTopRect;
	x2 = nRightRect;
	y2 = nBottomRect;

	this->type = DRAWRECT;
	this->color = color;
	this->penStyle = penStyle;
	
}


void CRectangle::Draw(HDC hdc)
{
	if (x1 < x2)
	{
		int tmp = x1;
		x1 = x2;
		x2 = tmp;
	}
	if (y1 < y2)
	{
		int tmp = y1;
		y1 = y2;
		y2 = tmp;
	}

	Rectangle(hdc, x1, y1, x2, y2);
}

ShapeMode CRectangle::getType()
{
	return this->type;
}

int  CRectangle::Inshape(int x, int y, HWND hWnd)
{


	RECT rc;

	HRGN hrgn = CreateRectRgn(x1, y1, x2, y2);


	// Test for a hit in the client rectangle.  

	if (PtInRegion(hrgn, x, y))
	{

		HDC hdc = GetDC(hWnd);
		Rectangle(hdc, x1 - 2, y1 - 2, x1 + 2, y1 + 2);
		Rectangle(hdc, x2 - 2, y2 - 2, x2 + 2, y2 + 2);
		Rectangle(hdc, x1 - 2, y2 - 2, x1 + 2, y2 + 2);
		Rectangle(hdc, x2 - 2, y1 - 2, x2 + 2, y1 + 2);

		if (((x1 - x)*(x1 - x) >= (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) >= (y2 - y)*(y2 - y)))
		{
			return 1;
		}
		else
		{
			if (((x1 - x)*(x1 - x) < (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) < (y2 - y)*(y2 - y)))
				return 3;
			else
			{

				if (((x1 - x)*(x1 - x) > (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) < (y2 - y)*(y2 - y)))
					return 2;
				else
				{
					if (((x1 - x)*(x1 - x) < (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) > (y2 - y)*(y2 - y)))
						return 4;
				}
			}
		}


	}

	return 0;
}
int  CLine::Inshape(int x, int y, HWND hWnd)
{
	float dx = abs(x1 - x2); //delta x
	float dy = abs(y1 - y2); //delta y
	float linelength = sqrtf(dx * dx + dy * dy);
	dx /= linelength;
	dy /= linelength;
	//dx va dy la vector chi phuong
	//vecto vuong goc la (-dy, dx)
	const float thickness = 10.0f; //Do lech click chuot
	const float px = 0.5f * thickness * (-dy); //vecto vuong goc voi do lech * 0.5
	const float py = 0.5f * thickness * dx;
	POINT pt[4]; // tao mang 4 dinh hinh cn cua duong thang
	pt[0].x = x1 - px; pt[0].y = y1 + py;
	pt[1].x = x2 - px; pt[1].y = y2 + py;
	pt[2].x = x2 + px; pt[2].y = y2 - py;
	pt[3].x = x1 + px; pt[3].y = y1 - py;
	HRGN hrgn = CreatePolygonRgn(pt, 4, ALTERNATE); // tao region 
	if (PtInRegion(hrgn, x, y))
	{
		HDC hdc = GetDC(hWnd);
		Rectangle(hdc, x1, y1, x1 - 5, y1 + 5);
		Rectangle(hdc, x2, y2, x2 - 5, y2 + 5);
		if (((x1 - x)*(x1 - x) >= (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) >= (y2 - y)*(y2 - y)))
		{
			return 1;
		}
		else
		{
			if (((x1 - x)*(x1 - x) <= (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) <= (y2 - y)*(y2 - y)))
				return 3;
			else
			{

				if (((x1 - x)*(x1 - x) >= (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) <= (y2 - y)*(y2 - y)))
					return 2;
				else
				{
					if (((x1 - x)*(x1 - x) <= (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) >= (y2 - y)*(y2 - y)))
						return 4;
				}
			}

		}
		return 1;

	}
	return 0;


}
int CEllipse::Inshape(int x, int y, HWND hWnd)
{

	RECT rc;


	HRGN hrgn = CreateEllipticRgn(x1, y1, x2, y2);

	
	// Test for a hit in the client rectangle.  


		if (PtInRegion(hrgn, x, y))
		{

			HDC hdc = GetDC(hWnd);
			Rectangle(hdc, x1 - 2, y1 - 2, x1 + 2, y1 + 2);
			Rectangle(hdc, x2 - 2, y2 - 2, x2 + 2, y2 + 2);
			Rectangle(hdc, x1 - 2, y2 - 2, x1 + 2, y2 + 2);
			Rectangle(hdc, x2 - 2, y1 - 2, x2 + 2, y1 + 2);
			if (((x1 - x)*(x1 - x) >= (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) == (y2 - y)*(y2 - y)))
			{
				return 1;
			}
			if (((x1 - x)*(x1 - x) >= (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) >= (y2 - y)*(y2 - y)))
			{
				return 1;
			}
			else
			{
				if (((x1 - x)*(x1 - x) <= (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) <= (y2 - y)*(y2 - y)))
					return 3;
				else
				{

					if (((x1 - x)*(x1 - x) >= (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) <= (y2 - y)*(y2 - y)))
						return 2;
					else
					{
						if (((x1 - x)*(x1 - x) <= (x2 - x)*(x2 - x)) && ((y1 - y)*(y1 - y) >= (y2 - y)*(y2 - y)))
							return 4;
						
					}
				}
			}
			
			return 1;
		
	}

	return 0;

}

CShape* CText::Create()
{
	return new CText;
}


void CText::SetData(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, COLORREF color, int penStyle)
{
	x1 = nLeftRect;
	y1 = nTopRect;
	x2 = nRightRect;
	y2 = nBottomRect;

	this->type = TEXT;
	this->color = color;
	this->penStyle = penStyle;

	
}


void CText::Draw(HDC hdc)
{
	SetBkMode(hdc, TRANSPARENT);
	SetTextColor(hdc, color);


	HFONT  hfontPrev;
	DWORD rgbPrev;
	// Initialize CHOOSEFONT
	HFONT font = CreateFontIndirect(&logfont);
	SelectObject(hdc,font);
	TextOut(hdc, x1, y1,str,length);
	return;

}

ShapeMode CText::getType()
{
	return this->type;
}
int CText::Inshape(int x, int y, HWND hWnd)
{
	return false;
}

void CText::setText(wchar_t* buff, LOGFONT* logfont, int length)
{

	
	this->logfont = *logfont;
	this->length = length;
	str = new wchar_t[length];
	
	for(int i=0; i<length; i++)
	{
		str[i] = buff[i];
	}
	return;
}
void CLine::setText(wchar_t* buff, LOGFONT* logfont, int length)
{
	
	return;
}
void CEllipse::setText(wchar_t* buff, LOGFONT* logfont, int length)
{
	
	return;
}
void CRectangle::setText(wchar_t* buff, LOGFONT* logfont, int length)
{
	return;
}

void CShape::Save(HANDLE hFile)
{
	DWORD dwBytesWritten;
	WriteFile(hFile, &type, sizeof(ShapeMode), &dwBytesWritten, NULL);
	WriteFile(hFile, &color, sizeof(COLORREF), &dwBytesWritten, NULL);
	RECT*t = this->getDimens();
	WriteFile(hFile, t, sizeof(RECT), &dwBytesWritten, NULL);
	WriteFile(hFile, &length, sizeof(DWORD), &dwBytesWritten, NULL);
	//WriteFile(hFile, &font, sizeof(HFONT), &dwBytesWritten, NULL);
	WriteFile(hFile, &str, sizeof(wchar_t)*length, &dwBytesWritten, NULL);
	return;

}
