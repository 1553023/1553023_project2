﻿#pragma once

#include<math.h>
//Enum để lựa chọn công cụ vẽ
enum ShapeMode
{
	LINE,
	DRAWRECT,
	DRAWELLIPSE,
	TEXT

};

struct Point
{
	int x;
	int y;
};

Point newPoint(int x, int y);

class CShape
{
protected:

	ShapeMode type = DRAWRECT;
	COLORREF color = NULL;
	int x1 = 0;
	int y1 = 0;
	int x2 = 0;
	int y2 = 0;


	int length = 0;
	wchar_t *str = NULL;

	LOGFONT logfont;
	
	int penStyle = 0;
	
	
	

public:


	virtual void Draw(HDC hdc) = 0;
	virtual CShape* Create() = 0;
	virtual void SetData(int a, int b, int c, int d, COLORREF color, int penStyle) = 0;
	virtual ShapeMode getType() = 0;
	void setType(ShapeMode type);
	COLORREF getColor();
	RECT* getDimens();
	void setDimens(RECT* rect);
	void setColor(COLORREF color);
	int getPenStyle();
	LOGFONT getfont()
	{
		return logfont;
	}
	wchar_t* gettext()
	{
		return this->str;
	}
	void getx1y1(int & x, int & y)
	{
		x = x1;
		y = y1;

	}
	void getx2y2(int & x, int & y)
	{
		x = x2;
		y = y2;

	}
	int getlenght()
	{
		return length;
	}
	virtual int  Inshape(int x, int y, HWND hWnd) = 0;
	virtual void setText(wchar_t* buff, LOGFONT* logfont, int length) = 0;
	void Save(HANDLE h);
};


class CLine : public CShape {
public:
public:
	void Draw(HDC hdc);
	CShape* Create();
	void SetData(int a, int b, int c, int d, COLORREF color, int penStyle);
	ShapeMode getType();
	int  Inshape(int x, int y, HWND hWnd);

	void setText(wchar_t* buff, LOGFONT* logfont, int length);
};


class CEllipse : public CShape {
private:
public:
	void Draw(HDC hdc);
	CShape* Create();
	void SetData(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, COLORREF color, int penStyle);
	ShapeMode getType();
	int  Inshape(int x, int y, HWND hWnd);

	void setText(wchar_t* buff, LOGFONT* logfont, int length);
};

class CRectangle : public CShape {
private:
public:
	void Draw(HDC hdc);
	CShape* Create();
	void SetData(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, COLORREF color, int penStyle);
	ShapeMode getType();
	int  Inshape(int x, int y, HWND hWnd);

	void setText(wchar_t* buff, LOGFONT* logfont, int length);
};

class CText : public CShape {
private:
public:
	void Draw(HDC hdc);
	CShape* Create();
	void SetData(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, COLORREF color, int penStyle);
	ShapeMode getType();
	int  Inshape(int x, int y, HWND hWnd);
	void setText(wchar_t* buff, LOGFONT* logfont, int length);
};

