﻿// 1553023_project2.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1553023_project2.h"


#include <windowsx.h>
#include <vector>
#include <string>
#include <fstream>
#include <Commdlg.h>
#include <commctrl.h>

using namespace std;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name




HMENU hmenu;

HWND g_hWnd;
HWND hStatusBar;
Point p1, p2;
bool isEdit = false;
int choose;
ShapeMode currShapeMode = DRAWELLIPSE;
WORD length;
ShapeMode lastShape;
vector<CShape*> shapes;
COLORREF lastColor = RGB(0, 0, 0);
COLORREF lastColorLine= RGB(0, 0, 0);
CShape* currShape = NULL;
COLORREF currColor = RGB(0, 0, 0);
COLORREF currColorLINE = RGB(0, 0, 0);
int currPenSize = 1;
int currPenStyle = PS_SOLID;
HPEN hPen = CreatePen(currPenStyle, currPenSize, currColor);
int windowWidth = 1080, windowHeight = 720;
string defaultFilePath = "paint.drw";
string openfile;

int temp = 0;
int bitmap = 0;
int i = 0;
void initNewObject();
int open = 0;
int active = 0;
void ONLBUTTONDOWN(LPARAM lParam, int &x1, int&y1, int &x2, int &y2, HINSTANCE hInst, HWND hWnd);
void ONMOUSEMOVE(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int &x2, int &y2);

void saveToFile(string filePath);
void saveToBinaryFile(string filePath);
void loadFromBinaryFile(string filePath);
void openFileDialog();
void saveFileDialog();
bool MOVE = false;

bool save = false;
HFONT hfont;
LOGFONT logfont;
string path;

int change = 0;
wchar_t *buff;

int num = 0;

struct CHILD_WND_DATA
{
	int num;
	HWND  hBitmap;
	COLORREF rgbColor;
	COLORREF rgbColorLINE;
	HFONT font;
	ShapeMode Mode;
	vector<CShape*> shapes;
	string file;
	bool isave;
	
};
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndProcBITMAP(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    TextProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    SaveProc(HWND, UINT, WPARAM, LPARAM);


INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
ATOM MyRegisterClassBITMAP(HINSTANCE hInstance);

void CreateColorBox(HWND);
void CreateToolbar(HWND hWnd);
void doToolBar_NotifyHandle(LPARAM	lParam);
HWND hToolBarWnd;
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32
// TRONG NHAN **********************************8

#define IDM_FIRSTCHILD 50000

int sub = 0;





static DWORD rgbCurrent;
HMENU hMenuInit, hMenuTEXT, hMenuBITMAP;
HMENU hMenuInitWin, hMenuTEXTWin, hMenuBITMAPWin;
HMENU hMenu;

HWND hWndFrame;

BOOL CALLBACK CloseProc(HWND hWnd, LPARAM lParam);
//TRONG NHAN ************************************

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	hInst = hInstance;

	// Initialize global strings






	MyRegisterClass(hInstance);
	MyRegisterClassBITMAP(hInstance);

	InitInstance(hInstance, nCmdShow);

	hMenuInit = LoadMenu(hInstance, szWindowClass);

	hMenuInit = LoadMenu(hInstance, L"BITMAP");




	// Perform application initialization:



	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1553023_PROJECT2));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(hWndFrame, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	DestroyMenu(hMenuTEXT);
	DestroyMenu(hMenuBITMAP);

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1553023_PROJECT2));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_MY1553023_PROJECT2);
	wcex.lpszClassName = L"MYPAINT";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

ATOM MyRegisterClassBITMAP(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProcBITMAP;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 4;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1553023_PROJECT2));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_MY1553023_PROJECT2);
	wcex.lpszClassName = L"BITMAP";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWndClient;

	hWndFrame = CreateWindow(L"MYPAINT", L"MYPAINT", WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, hMenuInit, hInstance, NULL);


	hWndClient = GetWindow(hWndFrame, GW_CHILD);
	ShowWindow(hWndFrame, nCmdShow);
	UpdateWindow(hWndFrame);

	ShowWindow(hWndFrame, nCmdShow);
	UpdateWindow(hWndFrame);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
int xdis = 0;
int ydis = 0;

bool edit = false;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	static HWND hWndClient;
	CLIENTCREATESTRUCT clientcreate;
	HWND hWndChild;
	MDICREATESTRUCT mdicreate;


	switch (message)
	{
	case WM_CREATE:
	{

		clientcreate.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
		clientcreate.idFirstChild = IDM_FIRSTCHILD;
		hWndClient = CreateWindow(TEXT("MDICLIENT"), NULL, WS_CHILD | WS_CLIPCHILDREN | WS_VISIBLE | WS_CHILDWINDOW | WS_BORDER,
			0, 0, 0, 0, hWnd, (HMENU)1, hInst, (PSTR)&clientcreate);

		hMenu = GetMenu(hWnd);

		hmenu = GetMenu(hWnd);
		hmenu = LoadMenu(hInst, MAKEINTRESOURCE(IDC_MY1553023_PROJECT2 ));
		SetMenu(hWnd, hmenu);

		CheckMenuItem(hmenu, ID_DRAW_RECTANGLE, MF_CHECKED);
		CheckMenuItem(hmenu, ID_WINDOWS_CASCADE, MF_CHECKED);

		CreateToolbar(hWnd);
		return 0;

	}
	case WM_SIZE:
	{
		MoveWindow(hWndClient, 0, 26, LOWORD(lParam), HIWORD(lParam), TRUE);
		return 0;
	}
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_DRAW_TEXT:
		{

			currShapeMode = TEXT;
			lastShape = TEXT;

			CheckMenuItem(hmenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_ELLIPSE , MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_LINE , MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_TEXT, MF_CHECKED);
			break;
		}

		case ID_DRAW_FONT:
		{
			HWND hwnd;                // owner window
			HDC hdc;                  // display device context of owner window

			CHOOSEFONT cf;            // common dialog box structure

			HFONT  hfontPrev;
			DWORD rgbPrev;
			LOGFONT lf;
			// Initialize CHOOSEFONT
			ZeroMemory(&cf, sizeof(cf));
			cf.lStructSize = sizeof(cf);
			cf.hwndOwner = hWnd;
			cf.lpLogFont = &lf;

			cf.Flags = CF_SCREENFONTS | CF_EFFECTS;

			if (ChooseFont(&cf) == TRUE)
			{
				
				logfont = *cf.lpLogFont;
				hfont = CreateFontIndirect(&logfont);
				
				
			}



			break;
		}
		case ID_WINDOWS_CASCADE:
		{
			CheckMenuItem(hmenu, ID_WINDOWS_CASCADE, MF_CHECKED);
			CheckMenuItem(hmenu, ID_WINDOWS_TIDE , MF_UNCHECKED);
			SendMessage(hWndClient, WM_MDICASCADE, 0, 0);
			return 0;
		}
		case ID_WINDOWS_TIDE:
		{
			CheckMenuItem(hmenu, ID_WINDOWS_CASCADE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_WINDOWS_TIDE, MF_CHECKED);
			SendMessage(hWndClient, WM_MDITILE, 0, 0);
			return 0;
		}
		case ID_WINDOWS_CLOSEALL:
		{
			EnumChildWindows(hWndClient, CloseProc, 0);
			bitmap = 0;

			return 0;
		}
		case ID_DRAW_RECTANGLE:
		{
			currShapeMode = DRAWRECT;
			lastShape = currShapeMode;

			CheckMenuItem(hmenu, ID_DRAW_RECTANGLE, MF_CHECKED);
			CheckMenuItem(hmenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_LINE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_TEXT, MF_UNCHECKED);
			break;

		}



		case ID_DRAW_LINE:
		{

			currShapeMode = LINE;
			lastShape = currShapeMode;

			CheckMenuItem(hmenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_LINE, MF_CHECKED);
			CheckMenuItem(hmenu, ID_DRAW_TEXT, MF_UNCHECKED);
			break;

		}
		case ID_DRAW_ELLIPSE:
		{


			currShapeMode = DRAWELLIPSE;
			lastShape = currShapeMode;

			CheckMenuItem(hmenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_ELLIPSE, MF_CHECKED);
			CheckMenuItem(hmenu, ID_DRAW_LINE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_TEXT, MF_UNCHECKED);
			break;
		}
		case ID_DRAW_COLOR:
		{
			CreateColorBox(hWnd);
			break;
		}
		case ID_FILE_OPEN:
		{


			shapes.clear();
			bitmap++;
			num = bitmap;
			open = 1;
			TCHAR buff[250];
			wsprintf(buff, L"Nonname-%d.drw", bitmap);
			mdicreate.szClass = L"BITMAP";
			mdicreate.szTitle = buff;
			mdicreate.hOwner = hInst;
			mdicreate.x = CW_USEDEFAULT;
			mdicreate.y = CW_USEDEFAULT;
			mdicreate.cx = CW_USEDEFAULT;
			mdicreate.cy = CW_USEDEFAULT;
			mdicreate.style = 0;
			mdicreate, lParam = 0;
			hWndChild = (HWND)SendMessage(hWndClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdicreate);



			return 0;




		}
		case ID_FILE_SAVE:
		{
			MessageBox(hWnd, L"Ban da chon Save", L"Notice", MB_OK);

			if (save == false)
			{
				saveFileDialog();
				change = 1;
				save = true;
			}
			else
			{
				saveToBinaryFile(defaultFilePath);
				
			}


			

			break;

		}
		case ID_FILE_NEW:
		{

			bitmap++;
			num = bitmap;
			TCHAR buff[250];
			wsprintf(buff, L"Nonname-%d.drw", bitmap);
			mdicreate.szClass = L"BITMAP";
			mdicreate.szTitle = buff;
			mdicreate.hOwner = hInst;
			mdicreate.x = CW_USEDEFAULT;
			mdicreate.y = CW_USEDEFAULT;
			mdicreate.cx = CW_USEDEFAULT;
			mdicreate.cy = CW_USEDEFAULT;
			mdicreate.style = 0;
			mdicreate, lParam = 0;
			hWndChild = (HWND)SendMessage(hWndClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdicreate);
			return 0;
		}
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefFrameProc(hWnd, hWndClient, message, wParam, lParam);

		}
		break;

	case WM_NOTIFY:
	{
		doToolBar_NotifyHandle(lParam);
	}
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefFrameProc(hWnd, hWndClient, message, wParam, lParam);
	}
	return 0;
}


LRESULT CALLBACK WndProcBITMAP(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	HDC tdc;
	RECT rect;
	static int x1, x2, y1, y2;
	static int nSavedDC;
	static HWND text;
	
	switch (message)
	{

	case WM_CREATE:
	{

		struct CHILD_WND_DATA *wndData = (struct CHILD_WND_DATA *) VirtualAlloc(NULL, sizeof(struct CHILD_WND_DATA), MEM_COMMIT, PAGE_READWRITE);
		wndData->Mode = DRAWRECT;
		wndData->rgbColor = RGB(0, 0, 0);
		wndData->rgbColorLINE = RGB(0, 0, 0);
		wndData->font = (HFONT)GetStockObject(SYSTEM_FONT);
		wndData->hBitmap = hWnd;
		wndData->isave = false;
		save = false;
		wndData->num = num;
		shapes.clear();
		if (open == 1)
		{

			g_hWnd = hWnd;
			openFileDialog();
			hdc = GetDC(hWnd);

			for (int j = 0; j < shapes.size(); j++)
			{
				if (shapes[j]->getType() == LINE)
				{
					hPen = CreatePen(PS_SOLID, 2, shapes[j]->getColor());
				}
				else
				{
					hPen = CreatePen(PS_SOLID, 2, BLACK_PEN);
				}
				SelectObject(hdc, CreateSolidBrush(shapes[j]->getColor()));
				SelectObject(hdc, hPen);
				shapes[j]->Draw(hdc);
			}
			



			wndData->isave = true;
			wndData->file = openfile;
			save = true;
			defaultFilePath = openfile;

		}
		wndData->shapes = shapes;
		currColor = wndData->rgbColor;
		lastColor = wndData->rgbColor;
		currColorLINE = wndData->rgbColorLINE;
		lastColorLine = wndData->rgbColorLINE;
		SetWindowLong(hWnd, 0, (LONG)wndData);



		break;

	}

	case WM_COMMAND:
		if (open == 1) break;
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefMDIChildProc(hWnd, message, wParam, lParam);
		}
		break;

	case WM_MDIACTIVATE:
	{

		
		if (open == 1) break;
		struct CHILD_WND_DATA *wndData = (struct CHILD_WND_DATA *) GetWindowLong(hWnd, 0);

		currColor = wndData->rgbColor;
		lastColor = wndData->rgbColor;

		currShapeMode = wndData->Mode;
		lastShape = wndData->Mode;

		switch (currShapeMode)
		{
		case LINE:
		{

			CheckMenuItem(hmenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_LINE, MF_CHECKED);
			CheckMenuItem(hmenu, ID_DRAW_TEXT, MF_UNCHECKED);
			break;
		}
		case TEXT:
		{
			CheckMenuItem(hmenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_LINE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_TEXT, MF_CHECKED);
			break;

		}
		case DRAWRECT:
		{
			CheckMenuItem(hmenu, ID_DRAW_RECTANGLE, MF_CHECKED);
			CheckMenuItem(hmenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_LINE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_TEXT, MF_UNCHECKED);
			break;
		}
		case DRAWELLIPSE:
		{
			CheckMenuItem(hmenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_ELLIPSE, MF_CHECKED);
			CheckMenuItem(hmenu, ID_DRAW_LINE, MF_UNCHECKED);
			CheckMenuItem(hmenu, ID_DRAW_TEXT, MF_UNCHECKED);
			break;
		}
		}
		currColorLINE = wndData->rgbColorLINE;
		lastColorLine = wndData->rgbColorLINE;

		num = wndData->num;
		if (change == 1)
		{
			save = true;
			wndData->isave = save;
			wndData->file = defaultFilePath;
			
			change = 0;
			
		}
		else
		{
			save = wndData->isave;
			defaultFilePath = wndData->file;
		}
		SetWindowLong(hWnd, 0, (LONG)wndData);
		shapes = wndData->shapes;






		break;
	}


	case WM_RBUTTONDOWN:
	{
		if (open == 1) break;

		int x = LOWORD(lParam);
		int y = HIWORD(lParam);

		CHILD_WND_DATA *wndData = (struct CHILD_WND_DATA *) GetWindowLong(hWnd, 0);
		for (int j = 0 ;j<wndData->shapes.size() ; j++)
		{
			int Inregn = wndData->shapes[j]->Inshape(x, y, hWnd);
			if (Inregn>0)
			{
				choose = j;
				hdc = GetDC(hWnd);
				MOVE = true;
				SelectObject(hdc, CreatePen(PS_DOT, 1, BLACK_PEN));

				currPenStyle = PS_DOT;

				currShapeMode = wndData->shapes[j]->getType();
				currColor = wndData->shapes[j]->getColor();
				currColorLINE = wndData->shapes[j]->getColor();



			}
		}



		if (MOVE)
		{
			ONLBUTTONDOWN(lParam, p1.x, p1.y, p2.x, p2.y,hInst,hWnd);


			if (wndData->shapes[choose]->getType() == LINE)
			{
				wndData->shapes[choose]->getx1y1(p2.x, p2.y);
				wndData->shapes[choose]->getx2y2(p1.x, p1.y);
				xdis = p2.x - p1.x;
				ydis = p2.y - p1.y;
				break;
			}
			wndData->shapes[choose]->getx1y1(p2.x, p2.y);
			wndData->shapes[choose]->getx2y2(p1.x, p1.y);
			xdis = p2.x - p1.x;
			ydis = p2.y - p1.y;

		}
		break;
	}








	case WM_LBUTTONDOWN:
	{

		
		int Inregn;
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);
		CHILD_WND_DATA *wndData = (struct CHILD_WND_DATA *) GetWindowLong(hWnd, 0);
		for (int j =0;j< wndData->shapes.size(); j++)
		{
			Inregn = wndData->shapes[j]->Inshape(x, y, hWnd);
			if (Inregn>0 )
			{
				choose = j;
				hdc = GetDC(hWnd);
				isEdit = true;
				SelectObject(hdc, CreatePen(PS_DOT, 1, BLACK_PEN));

				currPenStyle = PS_DOT;

				currShapeMode = wndData->shapes[j]->getType();
				currColor = wndData->shapes[j]->getColor();
				currColorLINE = wndData->shapes[j]->getColor();



			}
		}

		if (open == 1) break;

		ONLBUTTONDOWN(lParam, p1.x, p1.y, p2.x, p2.y,hInst ,hWnd);

		if (isEdit)
		{
			if (wndData->shapes[choose]->getType() == LINE)
			{
				if (Inregn == 1)
				{
					wndData->shapes[choose]->getx2y2(p2.x, p2.y);
					wndData->shapes[choose]->getx1y1(p1.x, p1.y);
				}
				else
				{
					wndData->shapes[choose]->getx1y1(p2.x, p2.y);
					wndData->shapes[choose]->getx2y2(p1.x, p1.y);
				}
				break;
			}
			else
			{

				if (wndData->shapes[choose]->getType() == DRAWRECT)
				{
					switch (Inregn)
					{
					case 1:
					{
						wndData->shapes[choose]->getx1y1(p1.x, p1.y);
						wndData->shapes[choose]->getx2y2(p2.x, p2.y);
						break;
					}
					case 2:
					{
						wndData->shapes[choose]->getx1y1(p1.x, p2.y);
						wndData->shapes[choose]->getx2y2(p2.x, p1.y);
						break;

					}

					case 3:
					{
						wndData->shapes[choose]->getx1y1(p2.x, p2.y);
						wndData->shapes[choose]->getx2y2(p1.x, p1.y);
						break;
					}
					case 4:
					{
						wndData->shapes[choose]->getx1y1(p2.x, p1.y);
						wndData->shapes[choose]->getx2y2(p1.x, p2.y);
						break;
					}
					}




					break;
				}


				else
				{
					if (wndData->shapes[choose]->getType() == DRAWELLIPSE)
					{
						switch (Inregn)
						{
						case 1:
						{
							wndData->shapes[choose]->getx1y1(p1.x, p1.y);

							

							wndData->shapes[choose]->getx2y2(p2.x, p2.y);
							
							
							break;
						}
						case 2:
						{
							wndData->shapes[choose]->getx1y1(p1.x, p2.y);
							wndData->shapes[choose]->getx2y2(p2.x, p1.y);
							break;

						}

						case 3:
						{
							wndData->shapes[choose]->getx1y1(p2.x, p2.y);
				
							wndData->shapes[choose]->getx2y2(p1.x, p1.y);
							break;
						}
						case 4:
						{
							wndData->shapes[choose]->getx1y1(p2.x, p1.y);
							wndData->shapes[choose]->getx2y2(p1.x, p2.y);
							break;
						}
						}




						break;


					}
				}
			}
			
		}
		

		break;
	}

	case WM_MOUSEMOVE:
	{
		if (open == 1) break;
		ONMOUSEMOVE(hWnd, wParam, lParam, p1.x, p1.y, p2.x, p2.y);
		break;
	}

	case WM_RBUTTONUP:
	{
		CHILD_WND_DATA *wndData = (struct CHILD_WND_DATA *) GetWindowLong(hWnd, 0);
		if (MOVE == false) break;
		if (MOVE)
		{
			for (int k = choose; k < wndData->shapes.size() - 1; k++)
			{
				wndData->shapes[k] = wndData->shapes[k + 1];
			}
			wndData->shapes.pop_back();

		}

		currPenStyle = PS_SOLID;
		currColorLINE = wndData->rgbColorLINE;
		InvalidateRect(hWnd, NULL, TRUE);
		if (open == 1) break;


		switch (currShapeMode)
		{

		case LINE:
		{
			CLine* line = new CLine;
			line->SetData(p2.x - xdis, p2.y - ydis, p2.x, p2.y,  currColorLINE, currPenStyle);
			wndData->shapes.push_back(line);
		}

		break;
		case DRAWRECT:
		{
			CRectangle* rect = new CRectangle;
			rect->SetData(p2.x - xdis, p2.y - ydis, p2.x, p2.y, currColor, currPenStyle);
			wndData->shapes.push_back(rect);
		}
		break;
		case DRAWELLIPSE:
		{
			CEllipse* ellipse = new CEllipse;
			ellipse->SetData(p2.x - xdis, p2.y - ydis, p2.x, p2.y, currColor, currPenStyle);
			wndData->shapes.push_back(ellipse);
		}
		}
		currShapeMode = lastShape;
		MOVE = false;
		currColor = lastColor;
		currColorLINE = lastColorLine;



		SetWindowLong(hWnd, 0, (LONG)wndData);




		break;


	}
	case WM_LBUTTONUP:
	{
		CHILD_WND_DATA *wndData = (struct CHILD_WND_DATA *) GetWindowLong(hWnd, 0);


		if (isEdit)
		{
			for (int k = choose; k < wndData->shapes.size() - 1; k++)
			{
				wndData->shapes[k] = wndData->shapes[k + 1];
			}
			wndData->shapes.pop_back();


		}

		currPenStyle = PS_SOLID;
		InvalidateRect(hWnd, NULL, TRUE);
		if (open == 1) break;


		switch (currShapeMode)
		{
		case TEXT:
		{
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, TextProc);
			if (length <= 0) break;
			hdc = GetDC(hWnd);
			CText* txt = new CText;
			txt->SetData(p1.x, p1.y, p2.x, p2.y, lastColor, currPenStyle);
			txt->setText(buff, &logfont, length);
			wndData->shapes.push_back(txt);
			txt->Draw(hdc);
			
			break;
			
		}
		case LINE:
		{
			CLine* line = new CLine;
			line->SetData(p2.x, p2.y, p1.x, p1.y, currColorLINE, currPenStyle);
			wndData->shapes.push_back(line);
		}
		break;
		case DRAWRECT:
		{
			CRectangle* rect = new CRectangle;
			rect->SetData(p1.x, p1.y, p2.x, p2.y, currColor, currPenStyle);
			wndData->shapes.push_back(rect);
		}
		break;
		case DRAWELLIPSE:
		{
			CEllipse* ellipse = new CEllipse;
			ellipse->SetData(p1.x, p1.y, p2.x, p2.y, currColor, currPenStyle);
			wndData->shapes.push_back(ellipse);
		}
		}
		currShapeMode = lastShape;
		isEdit = false;
		
		CheckMenuItem(hmenu, ID_EDIT_EDIT, MF_UNCHECKED);
		currColor = lastColor;
		currColorLINE = lastColorLine;
		wndData->rgbColor = currColor;
		wndData->rgbColorLINE = currColorLINE;
		wndData->Mode = currShapeMode;

		shapes = wndData->shapes;
		SetWindowLong(hWnd, 0, (LONG)wndData);




		break;

	}

	case WM_PAINT:
	{


		hdc = BeginPaint(hWnd, &ps);
		CHILD_WND_DATA *wndData = (struct CHILD_WND_DATA *) GetWindowLong(hWnd, 0);
		
		
		if (change == 1)
		{
			save = true;
			wndData->isave = save;
			wndData->file = defaultFilePath;

			change = 0;

		}
		if (open != 1)
		{

			struct CHILD_WND_DATA *wnddata = (struct CHILD_WND_DATA *) GetWindowLong(hWnd, 0);

			currColor = wnddata->rgbColor;
			currColorLINE = wnddata->rgbColorLINE;
			currShapeMode = wnddata->Mode;

			shapes.clear();
			shapes = wnddata->shapes;


		}
		open = 0;
		

		

		for (int j = 0; j < wndData->shapes.size(); j++)
		{
			if (wndData->shapes[j]->getType() == LINE)
			{
				hPen = CreatePen(PS_SOLID, 1, wndData->shapes[j]->getColor());
			}
			else
			{
				hPen = CreatePen(PS_SOLID, 1, BLACK_PEN);
			}
			SelectObject(hdc, CreateSolidBrush(shapes[j]->getColor()));
			SelectObject(hdc, hPen);
			wndData->shapes[j]->Draw(hdc);
		}


		wndData->Mode = currShapeMode;
		wndData->rgbColor = currColor;
		wndData->rgbColorLINE = currColorLINE;
		shapes = wndData->shapes;
		save = wndData->isave;
		defaultFilePath = wndData->file;
		SetWindowLong(hWnd, 0, (LONG)wndData);



		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	}
	case WM_CLOSE:
	{
		DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG2), hWnd, SaveProc);

		SendMessage(GetParent(hWnd), WM_MDIDESTROY, (WPARAM)hWnd, 0);
		
		break;
	}
	case WM_DESTROY:
	{
		
		break;
	}
	break;
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK SaveProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK)
		{
			if (save == true)
			{
				saveToBinaryFile(defaultFilePath);
			}
			else
			{
				saveFileDialog();
			}
			
			EndDialog(hDlg, LOWORD(wParam));
			MessageBox(NULL, L"SAVE successfully", L"Notice", MB_OK);
			return (INT_PTR)TRUE;
		}
		if (LOWORD(wParam) == IDCANCEL)
		{

			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
			
		}
		break;
	}
	return (INT_PTR)FALSE;
}
INT_PTR CALLBACK TextProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK )
		{
			buff = new wchar_t[256];
			length = (WORD)SendDlgItemMessage(hDlg,
				IDC_EDIT1,
				EM_LINELENGTH,
				(WPARAM)0,
				(LPARAM)0);
			length++;
			GetDlgItemText(hDlg, IDC_EDIT1, buff, length);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		if (LOWORD(wParam) == IDCANCEL)
		{
			buff = NULL;
			length = 0;
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
BOOL CALLBACK CloseProc(HWND hWnd, LPARAM lParam)
{
	if (GetWindow(hWnd, GW_OWNER)) return TRUE;
	SendMessage(GetParent(hWnd), WM_MDIRESTORE, (WPARAM)hWnd, 0);
	if (!SendMessage(hWnd, WM_QUERYENDSESSION, 0, 0))
		return TRUE;
	SendMessage(GetParent(hWnd), WM_MDIDESTROY, (WPARAM)hWnd, 0);
	return TRUE;
}

void CreateColorBox(HWND hWnd)
{
	CHOOSECOLOR ColorBox;
	static COLORREF acrCustClr[16];
	HBRUSH hbrush;



	SecureZeroMemory(&ColorBox, sizeof(ColorBox));
	ColorBox.lStructSize = sizeof(ColorBox);
	ColorBox.hwndOwner = hWnd;
	ColorBox.lpCustColors = (LPDWORD)acrCustClr;
	ColorBox.rgbResult = rgbCurrent;
	ColorBox.Flags = CC_NONE | CC_RGBINIT;

	if (ChooseColor(&ColorBox) == TRUE)
	{
		hbrush = CreateSolidBrush(ColorBox.rgbResult);
		rgbCurrent = ColorBox.rgbResult;
		currColor = rgbCurrent;
		currColorLINE = rgbCurrent;
		lastColor = currColor;
		lastColorLine = currColorLINE;
	}

}
void CreateToolbar(HWND hWnd)
{
	// loading Common Control DLL
	InitCommonControls();

	TBBUTTON tbButtons[] =
	{
		// Zero-based Bitmap image, ID of command, Button state, Button style, 
		// ...App data, Zero-based string (Button's label)
		{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },

	};

	// create a toolbar
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));


	// define new buttons
	TBBUTTON tbButtons11[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 0, ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 1, ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 2, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 3, ID_DRAW_TEXT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP1 };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbButtons11[1].iBitmap += idx;
	tbButtons11[2].iBitmap += idx;
	tbButtons11[3].iBitmap += idx;
	tbButtons11[4].iBitmap += idx;


	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons11) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons11);

}
void doToolBar_NotifyHandle(LPARAM	lParam)
{

	LPTOOLTIPTEXT   lpToolTipText;
	TCHAR			szToolTipText[TOOL_TIP_MAX_LEN]; 	// ToolTipText, loaded from Stringtable resource

														// lParam: address of TOOLTIPTEXT struct
	lpToolTipText = (LPTOOLTIPTEXT)lParam;

	if (lpToolTipText->hdr.code == TTN_NEEDTEXT)
	{
		// hdr.iFrom: ID cua ToolBar button -> ID cua ToolTipText string
		LoadString(hInst, lpToolTipText->hdr.idFrom, szToolTipText, TOOL_TIP_MAX_LEN);

		lpToolTipText->lpszText = szToolTipText;
	}
}






void ONLBUTTONDOWN(LPARAM lParam, int &x1, int&y1, int &x2, int &y2, HINSTANCE hInst,HWND hWnd)
{
	p1 = newPoint(LOWORD(lParam), HIWORD(lParam));
	p2 = newPoint(LOWORD(lParam), HIWORD(lParam));


	initNewObject();


}
void ONMOUSEMOVE(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int &x2, int &y2)
{

	if ((wParam & MK_RBUTTON) == MK_RBUTTON)
	{
		if (MOVE == true)
		{
			HDC hdc = GetDC(hWnd);

			HPEN pen;


			SetROP2(hdc, R2_NOTXORPEN);



			currShape->setType(currShapeMode);
			if (currShapeMode == LINE)
			{
				pen = CreatePen(currPenStyle, 1, currColorLINE);


				currColor = currColorLINE;

			}
			else
				pen = CreatePen(currPenStyle, 1, BLACK_PEN);

			SelectObject(hdc, pen);
			SelectObject(hdc, CreateSolidBrush(currColor));

			currShape->SetData(p2.x, p2.y, p2.x - xdis, p2.y - ydis, currColor, currPenStyle);
			currShape->Draw(hdc);


			p2 = newPoint(LOWORD(lParam), HIWORD(lParam));
			MoveToEx(hdc, p2.x, p2.y, 0);
			currShape->SetData(p2.x, p2.y, p2.x - xdis, p2.y - ydis, currColor, currPenStyle);
			currShape->Draw(hdc);


			ReleaseDC(hWnd, hdc);

			DeleteObject(pen);

		}
	}
	if ((wParam & MK_LBUTTON) == MK_LBUTTON)
	{
		HDC hdc = GetDC(hWnd);

		HPEN pen;


		SetROP2(hdc, R2_NOTXORPEN);



		currShape->setType(currShapeMode);







		MoveToEx(hdc, p1.x, p1.y, NULL);
		if (currShapeMode == LINE)
		{
			pen = CreatePen(currPenStyle, 1, currColorLINE);




		}
		else
			pen = CreatePen(currPenStyle, 1, BLACK_PEN);

		SelectObject(hdc, pen);
		SelectObject(hdc, CreateSolidBrush(currColor));

		currShape->SetData(p2.x, p2.y, p1.x, p1.y, currColor, currPenStyle);
		currShape->Draw(hdc);


		p2 = newPoint(LOWORD(lParam), HIWORD(lParam));
		MoveToEx(hdc, p2.x, p2.y, 0);
		currShape->SetData(p2.x, p2.y, p1.x, p1.y, currColor, currPenStyle);
		currShape->Draw(hdc);


		ReleaseDC(hWnd, hdc);

		DeleteObject(pen);


	}
	else return;
}
void initNewObject()
{
	switch (currShapeMode)
	{
	case LINE:
		currShape = new CLine();
		break;
	case DRAWRECT:
		currShape = new CRectangle();
		break;
	case DRAWELLIPSE:
		currShape = new CEllipse();
		break;
	case TEXT:
		currShape = new CText();
	default:
		break;
	}
}


void saveToFile(string filePath)
{
	ofstream myfile;
	myfile.open(filePath, ios::binary);

	if (!myfile.is_open())
	{
		MessageBox(g_hWnd, L"Error open file!", L"My Paint", MB_OK | MB_ICONWARNING);
		return;
	}

	//ok, proceed with output
	int l = shapes.size();
	myfile.write((char*)&l, sizeof(l));

	for (int i = 0; i < shapes.size(); i++)
	{
		myfile.write(reinterpret_cast<const char*>(shapes[i]), sizeof(CShape));
	}
	myfile.close();


	//Read
	ifstream inFile;
	inFile.open(filePath, ios::binary);

	if (!inFile.is_open())
	{
		MessageBox(g_hWnd, L"Error open file!", L"My Paint", MB_OK | MB_ICONWARNING);
		return;
	}
	int len = 0;
	inFile.read((char*)&len, sizeof(int));

	while (!inFile.eof() && len > 0)
	{
		char* item_buff = new char[255];
		CShape* tmp;
		inFile.read(item_buff, sizeof(CShape));
		tmp = reinterpret_cast<CShape*>(item_buff);
		//Workaround!
		if (tmp == NULL || &tmp == NULL)
		{
			break;
		}

		switch (tmp->getType())
		{
		case ID_DRAW_LINE:
			shapes.push_back(((CLine*)tmp));
			break;
		case ID_DRAW_RECTANGLE:
			shapes.push_back(((CRectangle*)tmp));
			break;
		case ID_DRAW_ELLIPSE:
			shapes.push_back(((CEllipse*)tmp));
			break;


		default:
			break;
		}

		len--;
	}
	inFile.close();
}


void saveToBinaryFile(string filePath)
{
	std::ofstream out;
	out.open(filePath, std::iostream::out | std::iostream::binary | std::iostream::trunc);

	if (out.is_open()) {
		int size = shapes.size();
		out.write(reinterpret_cast<const char *>(&size), sizeof(size));

		for (CShape* shape : shapes)
		{
			int type = shape->getType();
			COLORREF color = shape->getColor();
			RECT* rect = shape->getDimens();
			int lenght = shape->getlenght();
			wchar_t* str = shape->gettext();
			LOGFONT logfont = shape->getfont();
			LOGFONT *plog = &logfont;
			out.write(reinterpret_cast<const char*>(&type), sizeof(type));
			out.write(reinterpret_cast<const char*>(&color), sizeof(COLORREF));
			out.write(reinterpret_cast<const char*>(rect), sizeof(RECT));
			if (type == TEXT)
			{
				out.write(reinterpret_cast<const char*>(&lenght), sizeof(int));
				
				for (int i = 0; i < lenght; i++)
				{
					wchar_t c = str[i];
					out.write(reinterpret_cast<const char*>(&c), sizeof(wchar_t));
				}
				out.write(reinterpret_cast<const char*>(plog), sizeof(LOGFONT));
			}
		}
	}
	else {
		OutputDebugString(L"Can't open data.bin to write");
	}

	out.close();
}

void loadFromBinaryFile(string filePath) {
	std::ifstream in;
	in.open(filePath, std::iostream::in | std::iostream::binary);

	if (in.is_open()) {
		char* buffer = new char[255];
		int size;
		in.read(buffer, sizeof(size));

		size = buffer[0];
		shapes.clear();


		for (int i = 0; i < size; i++)
		{
			char* item_buff = new char[255];

			CShape* shape = NULL;

			int type;
			COLORREF color;
			in.read(item_buff, sizeof(type));
			type = item_buff[0];
			in.read(item_buff, sizeof(COLORREF));
			color = item_buff[0];
			int r = GetRValue(color);
			color = item_buff[1];
			int g = GetGValue(color);
			color = item_buff[2];
			int b = GetBValue(color);

			color = RGB(r, g, b);

			switch (type)
			{
			case LINE:
				shape = new CLine();
				break;
			case DRAWRECT:
				shape = new CRectangle();
				break;
			case DRAWELLIPSE:
				shape = new CEllipse();
				break;
			case TEXT:
				shape = new CText();

			default:
				break;
			}

			shape->setType((ShapeMode)type);
			shape->setColor(color);

			RECT* rect;
			in.read(item_buff, sizeof(RECT));
			rect = reinterpret_cast<RECT*>(item_buff);
			shape->setDimens(rect);

			if ((ShapeMode)type == TEXT)
			{
				int lenght;
				in.read(item_buff, sizeof(int));
				lenght = item_buff[0];

				wchar_t* str = new wchar_t[lenght];
				for (int i =0; i < lenght; i++)
				{
					wchar_t c;
					in.read(item_buff, sizeof(wchar_t));
					c = (wchar_t)(item_buff[0]);
					str[i] = c;
				}

				
				LOGFONT* font;
				in.read(item_buff, sizeof(LOGFONT));
				font = reinterpret_cast<LOGFONT*>(item_buff);
				shape->setText(str, font, lenght);
				
			}
			shapes.push_back(shape);

			delete[] item_buff;
			item_buff = NULL;
		}

		delete[] buffer;
	}
	else {

		OutputDebugString(L"Can't open data.drw to read");
	}

	in.close();
}
void openFileDialog()
{
	OPENFILENAME ofn;
	WCHAR szFilePath[MAX_PATH] = L"";
	WCHAR szFileTitle[MAX_PATH] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
	ofn.hwndOwner = g_hWnd;
	ofn.lpstrFilter = L"DRAW Files (*.drw)\0*.drw\0All Files (*.*)\0*.*\0";
	ofn.lpstrFile = szFilePath;
	ofn.lpstrFileTitle = szFileTitle;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = L"drw";

	if (GetOpenFileName(&ofn))
	{
		// Do something usefull with the filename stored in szFileName 
		wstring ws(szFilePath);
		// your new String
		string fileName(ws.begin(), ws.end());
		openfile = fileName;
		loadFromBinaryFile(fileName);
		InvalidateRect(g_hWnd, NULL, TRUE);
	}
}

void saveFileDialog()
{
	OPENFILENAME ofn;
	WCHAR szFileName[MAX_PATH] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	wsprintf(szFileName, L"Noname-%d.drw", num);
	ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
	ofn.hwndOwner = g_hWnd;
	ofn.lpstrFilter = L"DRAW Files (*.drw)\0*.drw\0All Files (*.*)\0*.*\0";
	ofn.lpstrFile = szFileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
	ofn.lpstrDefExt = L"drw";

	if (GetSaveFileName(&ofn))
	{
		// Do something usefull with the filename stored in szFileName 
		wstring ws(szFileName);
		// your new String
		string fileName(ws.begin(), ws.end());

		defaultFilePath = fileName;


		saveToBinaryFile(fileName);
	}
}